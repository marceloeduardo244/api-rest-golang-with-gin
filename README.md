# Api Rest Golang With Gin
- Projeto de uma Api com crud completo e Golang usando o ágil framework Gin

# Banco de dados
- Postgres
- PgAdmin
- Os dois rodam via docker, o arquivo docker-compose esta na raiz. (comando docker-compose up)

# Endpoints
- DELETE alunos/{id}
- GET alunos/{id}
- GET alunos
- POST alunos
{
	"nome": "Paula",
	"cpf": "12345678911",
	"rg": "544546457"
}
- PATCH alunos/{id}
{
	"nome": "Paula",
	"cpf": "12345678911",
	"rg": "544546457"
}

# Executando a aplicação
- Na raiz do projeto, go run main.go ou go build
