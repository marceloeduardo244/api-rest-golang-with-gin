package main

import (
	"gitlab.com/marceloeduardo244/api-rest-golang-with-gin/database"
	"gitlab.com/marceloeduardo244/api-rest-golang-with-gin/routes"
)

func main() {
	database.ConectaComBancoDeDados()
	r := routes.HandleRequests()
	r.Run()
}
