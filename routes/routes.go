package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/marceloeduardo244/api-rest-golang-with-gin/controller"
)

func HandleRequests() *gin.Engine {
	r := gin.Default()
	r.GET("/:nome", controller.Saudacao)
	r.GET("/alunos", controller.ExibeAlunos)
	r.GET("/alunos/:id", controller.ExibeAlunosPorId)
	r.GET("/alunos/cpf/:cpf", controller.BuscaAlunoPorCpf)
	r.POST("/alunos", controller.CriaNovoAluno)
	r.DELETE("/alunos/:id", controller.ApagarAluno)
	r.PATCH("/alunos/:id", controller.EditaAluno)

	return r
}
